import { query } from "nact";  

export default async (obj, args, ctx, info, type) => {
    const input = args.input
    const collectionItemActor = ctx.children.get('item');

    const result = await query(
        collectionItemActor, 
        { 
            type, 
            input 
        }, 
        process.env.ACTOR_TIMEOUT
    )
    //
    return {
        ...result,
        id: result._id
    }
}