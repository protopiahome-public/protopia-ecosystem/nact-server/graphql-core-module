import { query } from "nact"
import parseTypeByName from "./parseTypeByName"
import { QraphQLCoreError } from "../errors/QraphQLCoreError"

export default async (obj, args, ctx, info, collectionType, gqlType) => {
    const collectionActor = ctx.children.get('collection')
    // console.log( args )
    let projectsData =  await query(
        collectionActor, 
        { 
            type: collectionType,
            filter: args.filter,
            paging: args.paging,
            sort: args.sort,
        }, 
        process.env.ACTOR_TIMEOUT
    ) 
    if( !Array.isArray(projectsData) )
    {
        throw new QraphQLCoreError(`No one ${gqlType} not exists.`)
    }
    projectsData = projectsData.map(f => ({ ...f, id: f._id }))
    const ret = projectsData.map( fData => parseTypeByName( gqlType, ctx, fData, info ) ) 
    return ret
}