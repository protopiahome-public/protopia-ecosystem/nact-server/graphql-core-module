import { introspectionFromSchema } from "graphql";
import AppModules from "../../../modules"

export const getSchemaTypeByName = (typeName, ctx) => {
    const { schema } = AppModules(ctx);     
    return introspectionFromSchema(schema)
        .__schema
            .types
                .filter( t => t.name === typeName ) || []
}