import { query } from "nact";
import { getSchemaTypeByName } from "./getSchemaTypeByName"
import getReqFieldByTypeObject from "./getReqFieldByTypeObject";
import { FestivalError } from "../../fest-module/errors/FestivalError";
import _idToId from "./_idToId";
import idTo_id from "./idTo_id";

const parseTypeByName = async (typeName, ctx, data, info, level = 0 ) =>
{
    const type = getSchemaTypeByName( typeName, ctx )
    const collectionActor = ctx.children.get('collection');
    if(!type[0])
    {
        throw new FestivalError("No exists type: " + typeName)
    }
    /* Перечисляем все поля искомого типа */ 
    let func = async ( ret ) => 
    {
        // console.log("level: ", level)
        const f =  await Promise.all(type[0].fields
        .filter( field => {
            // При level === 0 пулучаем данные pfghjcf из Query или Mutation
            // Иначе - по-другому
            const selection = level === 0 
                ?
                info.fieldNodes[0].selectionSet.selections
                :
                info[0].selectionSet.selections
            const sf = selection.filter(ss => ss.name.value === field.name)
            return sf[0]
        } )
        .map( async (field, i) => { 
            // console.log( i, field.name )
            //if(level === 0 )
            //{
                // console.log( "   -", info.fieldNodes[level]?.selectionSet )
                
                /* Перечисляем все искомые поля запроса */                
                const selection = level === 0 
                    ?
                    info.fieldNodes[0].selectionSet.selections
                    :
                    info[0].selectionSet.selections 
                const sf = selection.filter(ss => ss.name.value === field.name)
                
                if( sf[0] && data )
                { 
                    if( level !== 0 )
                    {
                        //console.log( level, i, field.name )
                    }
                    /* если поле - SCALAR */
                    if( !sf[0].selectionSet )
                    {
                        if( field.name === "id")
                        {
                            data[field.name] = data._id
                        }
                        
                        ret[field.name] = await data[field.name]
                        return [];//[field.name, data[field.name]]
                    }
                    else                        
                    {
                        /* Получаем данные из базы. */
                        
                        // Название коллекции === тип данных со строчной первой буквы ( ОБЯЗВТЕЛЬНО!!! )
                        const dataTypeName =  getReqFieldByTypeObject( field.type ) 
                        let fltr = { type: tableNameCase(dataTypeName) } 

                        // Получаем дочерние объекты из МонгоДБ
                        if( data[field.name] )
                        {
                            // Если данные о связанном объекте пришли из коллекции
                            const search =  idTo_id( data[ field.name ] ) 
                            fltr.search = search 
                        } 
                        else
                        {
                            // Если в коллекции поля нет, а в схеме -- есть, то цепляем коллекцию из базы, фильтруя по полю [ field.name + "Id"]
                            fltr.search = {}
                            fltr.search[tableNameCase(typeName) + ".id"] = data._id.toString()
                        }
                        let chData = await query(
                            collectionActor, 
                            fltr, 
                            process.env.ACTOR_TIMEOUT
                        )
                        chData = chData.map(chd => ({ ...chd, id: chd._id }))  
                        // console.log( i, sf[0].name.value )
                        // console.log( "      ---", data[field.name] )

                        // console.log(  "     kind::", field.type.kind )
                        // console.log(  " ::", field.type )
                        // console.log(  "     :::",  tableNameCase(dataTypeName), sf[0].name.value)

                        // console.log( sf[0].selectionSet )
                        //sf[0].selectionSet.selections.forEach(sfs => {
                        //  console.log("            ", sfs.name.value) 
                        //}) 
                        chData = await field.type.kind === "OBJECT" ? chData[0] : chData
                        
                        if( field.type.kind === "OBJECT" )
                        {
                            // console.log( level ) 
                            // console.log( field ) 
                            // console.log( info )
                            // console.log( sf[0].selectionSet.selections )
                            // parseTypeByName( dataTypeName, ctx, chData, sf, level + 1 )
                        }
                        // console.log( "Имя поля: ", field.name ) 
                        // console.log( "Фильтре выборки: ", fltr ) 
                        // console.log( "Имя типа данных: ", dataTypeName ) 
                        // console.log( "Данные: ", chData ) 
                        // console.log( "" ) 

                        ret[field.name] = chData 
                        return [field.name, chData]                    
                    }
                }
                else
                {
                    return null
                }
            //}
        }))
        return ret
    }
    let ret = {}
    await func(ret) 
    // console.log( ret )
    return ret
}

const parseFiled = ( field, ctx, info, level ) =>
{
    const l = Array(level).fill(1).map(f => "   ").join("") 
    // console.log(l, field.name)
    // console.log(l, field.type.kind)
    // console.log(l, field.type.name)
    if( field.type.kind === "NON_NULL" )
    {
        // console.log( "KIND:", field.type.ofType.kind ) 
    }
    if(field.type.kind === "LIST")
    {
        //console.log( "LIST:" )  
        //console.log( field.type )  
        //parseFiled( field.type.ofType )  

    }
    if( field.type.kind === "OBJECT" )
    {
        //console.log( "OBJECT:", level + 1 )    
        parseTypeByName( field.type.name, {}, ctx, info, level + 1 )  
    }
    // console.log( l, " -- finifed: " + field.name)
}

export default parseTypeByName 

const tableNameCase = string => string[0].toLowerCase() + string.slice(1) 