import { query } from "nact"
import { QraphQLCoreError } from "../errors/QraphQLCoreError"

export default async ( obj, args, ctx, info, collectionType ) => {
    const collectionActor = ctx.children.get('collection')
    
    let count =  await query(
        collectionActor, 
        { 
            type: collectionType, 
            count: "count" 
        }, 
        process.env.ACTOR_TIMEOUT
    ) 
    return count.length === 0 
        ? 
        0 
        : 
        count[0].id
}