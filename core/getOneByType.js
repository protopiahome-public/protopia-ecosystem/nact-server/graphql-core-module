import { query } from "nact";
import parseTypeByName from "./parseTypeByName";

export default async (obj, args, ctx, info, type, gqlType) => {
    const collectionActor = ctx.children.get('item') 
    let search = {}
    Object.keys( args ).forEach(key => {
        let k = key === "id" ? "_id" : key
        search[k] = args[key]
    })
    let one =  args.id === "new"
        ?
        { status: "PUBLISH", title: `new ${type}` }
        :
        await query(
            collectionActor, 
            { 
                type,
                search, 
                input: args.input
            }, 
            process.env.ACTOR_TIMEOUT
        )
    
    one = !!one
        ?
        { ...one, id: one._id }
        :
        { }
    return parseTypeByName( gqlType, ctx, one, info )
}