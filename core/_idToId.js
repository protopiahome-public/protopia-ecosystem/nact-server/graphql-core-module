export default obj =>
{
    if(obj._id)
    {
        obj.id = obj._id
        delete obj._id
    }
    return obj
}