export default (reqFieldTypeObj = {}) => { 
    if( reqFieldTypeObj.kind === "OBJECT")
    {
        return  (reqFieldTypeObj.name) 
    }
    if(reqFieldTypeObj.kind === "LIST")
    {
         return getByName(reqFieldTypeObj.ofType)
    }
    return ""
}

const getByName = name =>
{
    if( name.kind === "OBJECT")
    {
        return name.name
    }
    if(name.kind === "LIST")
    {
        if(name.typeOf.kind === "OBJECT")
        {
            return name.typeOf.name
        }
        if(name.typeOf.kind === "NONE_NULL")
        {
            return name.typeOf.name
        }
    }
    return ""
}