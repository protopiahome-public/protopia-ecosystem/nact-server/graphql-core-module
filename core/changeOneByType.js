import { query } from "nact"

export default async (obj, args, ctx, info, type, gqlType) => {
    const collectionItemActor = ctx.children.get('item');
    
    const result = await query(
        collectionItemActor, 
        { 
            type, 
            search: { _id: args.id }, 
            input: args.input 
        }, 
        process.env.ACTOR_TIMEOUT
    );
    //
    return {
        ...result,
        id: result._id
    }
}