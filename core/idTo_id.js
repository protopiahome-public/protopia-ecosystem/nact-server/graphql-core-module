export default obj =>
{
    if(obj.id)
    {
        obj._id = obj.id
        delete obj.id
    }
    return obj
}