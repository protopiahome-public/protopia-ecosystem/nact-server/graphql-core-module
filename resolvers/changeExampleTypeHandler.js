import { ModuleSessionInfo } from "@graphql-modules/core"
import ExampleTypeResponse from "../response/ExampleTypeResponse"

const crypto = require('crypto')

export default ( obj, args, ctx, info ) =>
{
    // console.log( "obj:" )
    // console.log( obj )
    // console.log( "args:" )
    // console.log( args )
    // console.log( "ctx:" )
    // console.log( ctx )
    console.log( "info:" )
    console.log( info )
    // console.log( ModuleSessionInfo )
    return ExampleTypeResponse({
        id: args.input.id || 23,
        date: new Date(),
        email: "gg@hh.tu",
        url: "https://ya.ru",
        color: "red",
        char: "Karabas",
        media: {
            id: -1,
            url: "https://i.pinimg.com/236x/db/ef/c0/dbefc0127e7025e3616d51e0989907ba.jpg",
            name: "i.pinimg.com/236x/db/ef/c0/dbefc0127e7025e3616d51e0989907ba",
            mime: "image/jpeg",
            ext: "jpg"
        },
        json: { 
            ...args.input.json,
            'aa': crypto.randomBytes(7).toString('hex'), 
            'bb': [ crypto.randomBytes(4).toString('hex'), crypto.randomBytes(4).toString('hex') ] 
        }
    })
}