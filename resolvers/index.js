const { default: changeExampleTypeHandler } = require("./changeExampleTypeHandler");
const { default: getExampleTypeHandler } = require("./getExampleTypeHandler");

/*
*   This is example of resolvers.
*   Change this file structure for your tasks
*/
module.exports = {
    Mutation: { 
      changeExampleType: async (obj, args, ctx, info) => changeExampleTypeHandler(obj, args, ctx, info)
    },
    Query: { 
      getExampleType: async (obj, args, ctx, info) => getExampleTypeHandler(obj, args, ctx, info)
    }
  };