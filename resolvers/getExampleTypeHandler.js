import ExampleTypeResponse from "../response/ExampleTypeResponse"


export default ( obj, args, ctx, info ) =>
{
    return ExampleTypeResponse({
        id: -1,
        date: new Date(),
        email: "gg@hh.tu",
        url: "https://ya.ru",
        color: "red",
        char: "Karabas",
        media: {
            id: -1,
            url: "https://i.pinimg.com/236x/db/ef/c0/dbefc0127e7025e3616d51e0989907ba.jpg",
            name: "i.pinimg.com/236x/db/ef/c0/dbefc0127e7025e3616d51e0989907ba",
            mime: "image/jpeg",
            ext: "jpg"
        },
        json: { 'aa': 'yadern' }
    })
}