import { ApolloError } from "apollo-server";

export class QraphQLCoreError extends ApolloError {
    constructor(message ) {
        super(message, 'GRAPHQL_CORE_ERROR');
        Object.defineProperty(this, 'name', { value: 'QraphQLCoreError: ' });
    }
} 