<p align="center">
  <a href="https://nact.xyz/" target="blank"><img src="https://nact.xyz/logos/logo.svg" width="200" alt="ACT Logo" /></a>
</p> 


# graphql-module

Nact module to ProtopiaEcosystem.
A set of utilities for constructing resolvers from graphQL schema data. Forming data from MorgoDB for nested queries
